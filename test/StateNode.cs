﻿using System;
using System.Collections.Generic;



namespace testProject
{
    public enum StateNodeType { Or, Initial, End, History, Selection, Machine, NonTarget };

    public class StateNode
    {
        StateNodeArray sns;
        EA.DiagramObject dobj;
        EA.Element eState;
        short andID;    // どのパーディションに所属するか
        double weight;  // Orの時に重みを設定する
        public int ElementID { get { return eState.ElementID; } }
        public bool HasAndStates { get { return eState.Partitions.Count > 0; } }

        public short AndID {
            set { this.andID = value; }
            get { return this.andID; }
        }
        public double Weight
        {
            set { this.weight = value; }
            get { return this.weight; }
        }
        public string Name { get { return eState.Name; } }
        public string AndStateName(short aID)
        {
            if (aID >= 0 && aID < eState.Partitions.Count)
                return eState.Partitions.GetAt(aID).Name;
            return null;
        }

        public StateNode(StateNodeArray array, EA.DiagramObject diagramObject, EA.Element element)
        {
            sns = array;
            dobj = diagramObject;
            eState = element;

            weight = 1; // 大域重み
            AndID = 0;  // パーティション0
        }
        public StateNodeType Type()
        {
            if (eState.Type == "State")
            {
                return StateNodeType.Or;
            }
            else if (eState.Type == "StateMachine")
            {
                return StateNodeType.Machine;
            }
            else if (eState.Type == "StateNode")
            {
                if (eState.Subtype == 3 || eState.Subtype == 13 || eState.Subtype == 100)
                {
                    return StateNodeType.Initial;
                }
                else if (eState.Subtype == 4 || eState.Subtype == 12 || eState.Subtype == 101)
                {
                    return StateNodeType.End;
                }
                else if (eState.Subtype == 5 || eState.Subtype == 15)
                {
                    return StateNodeType.History;
                }
                else if (eState.Subtype == 11)
                {
                    return StateNodeType.Selection;
                }
            }
            return StateNodeType.NonTarget;
        }
        // 親要素がないか，あってもダイアグラム中にない場合Rootと判定する
        public bool IsRoot()
        {
            if (eState.ParentID == 0 || sns[eState.ParentID] == null)
            {
                return true;
            }
            return false;
        }
        // 子要素があり，タイヤグラム中にそれが存在していれば,Leafではない
        // それ以外は，Leafと判定する．
        // （Machineの時については一つの状態とみてその先を調べない：暫定的処理）
        public bool IsLeaf()
        {
            if (eState.Elements.Count > 0)
            {
                foreach (EA.Element ele in eState.Elements)
                {
                    if (sns[ele.ParentID] != null)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        public void AndSetting()
        {
            // Or状態かつパーティションが2以上のノードを対象とする．
            // 子状態を対象に，所属パーティションを決定する．
            if (Type() == StateNodeType.Or && eState.Partitions.Count > 0)
            {
                // 各子状態についてAndIDを設定する
                foreach (EA.Element ele in eState.Elements)
                {
                    StateNode child = sns[ele.ElementID];
                    if (child != null)
                    {
                        child.AndID = OwnerPartition(child);
                    }
                }
            }
        }
        // どのパーティションに入るかを決定する
        // dobj.botoomは,この状態の下部のY座標（マイナス値）
        // partition.Sizeを足すと，このパーティションの上部のY座標　かつ
        // 次のパーティションの下部のY座標になる．
        private short OwnerPartition(StateNode child)
        {
            int pBottom = dobj.bottom;
            short pID = eState.Partitions.Count;
            while (--pID >= 0)
            {
                EA.Partition partition = eState.Partitions.GetAt(pID);
                int pTop = pBottom + partition.Size;
                if (child.IsInside(pTop, pBottom) == true)
                {
                    return pID;
                }
                pBottom = pTop;
            }
            return 0;
        }
        // 状態がダイアグラムの中で，指定されたY座標値範囲に入るかどうか
        private bool IsInside(int pTop, int pBottom)
        {
            if (dobj.top < pTop && dobj.bottom > pBottom)
            {
                return true;
            }
            return false;
        }
        //再帰的にWeightを計算しSetする．
        public void SetWeight(double weight)
        {
            Weight = weight;
            System.Diagnostics.Debug.WriteLine(eState.Name + ": Weight="+Weight);

            if (IsLeaf() == false)
            {
                if (eState.Partitions.Count > 0)
                {
                    for (short pID = 0; pID < eState.Partitions.Count; pID++)
                    {
                        int nChild = 0;
                        foreach (EA.Element ele in eState.Elements)
                        {
                            StateNode sNode = sns[ele.ElementID];
                            if (sNode != null && sns.IsState(sNode) == true && sNode.AndID == pID)
                                nChild++;
                        }
                        foreach (EA.Element ele in eState.Elements)
                        {
                            StateNode sNode = sns[ele.ElementID];
                            if (sNode != null && sns.IsState(sNode) == true && sNode.AndID == pID)
                                sNode.SetWeight(Weight / nChild);
                        }
                    }
                }
                else {
                    int nChild = 0;
                    foreach (EA.Element ele in eState.Elements)
                    {
                        StateNode sNode = sns[ele.ElementID];
                        if (sNode != null && sns.IsState(sNode) == true)
                            nChild++;
                    }
                    foreach (EA.Element ele in eState.Elements)
                    {
                        StateNode sNode = sns[ele.ElementID];
                        if (sNode != null && sns.IsState(sNode) == true)
                            sNode.SetWeight(Weight / nChild);
                    }
                }
            }
        }
        // 親のWeightを参照（使うかどうか不明）
        public double ParentWeight()
        {
            if (eState.ParentID == 0 || sns[eState.ParentID] == null)
                return 1.0;
            else
                return sns[eState.ParentID].Weight;
        }
        // 親の要素IDを返す
        public StateNode ParentNode
        {
            get
            {
                if (eState.ParentID == 0 || sns[eState.ParentID] == null)
                    return null;
                else
                    return sns[eState.ParentID]; 
            }
        }

        // Or状態の内部の複雑度をみる
        public double CountStateComplexy(bool top=false)
        {
            double count = 0.0;
            if (IsLeaf() == false)
            {
                if (eState.Partitions.Count > 0)
                {
                    for (short pID = 0; pID < eState.Partitions.Count; pID++)
                    {
                        int nChild = 0;
                        foreach (EA.Element ele in eState.Elements)
                        {
                            StateNode sNode = sns[ele.ElementID];
                            if (sNode != null && sns.IsState(sNode) == true && sNode.AndID == pID)
                            {
                                nChild++;
                                count += sNode.CountStateComplexy();
                            }
  
                        }
                        count += Weight * (nChild - 1);
                    }
                }
                else
                {
                    int nChild = 0;
                    foreach (EA.Element ele in eState.Elements)
                    {
                        StateNode sNode = sns[ele.ElementID];
                        if (sNode != null && sns.IsState(sNode) == true)
                        {
                            nChild++;
                            count += sNode.CountStateComplexy();
                        }
                    }
                    count += Weight * (nChild - 1);
                }
            } 
            // この状態をトップだとすると，Weightで割り戻すことで，重み1にもどる．
            // その上で，「最初の状態」をカウントしていなかった分を足す．
            return (top == true) ? (count/Weight + 1.0) : count;
        }
    }

    public class StateNodeArray
    {
        StateNode[] array;
        public int length;
        Dictionary<int, int> e2d = new Dictionary<int, int>();

        // 状態とみなすかどうか
        bool isEnd_aState;
        bool isHistory_aState;
        bool isInitial_aState;

        public StateNodeArray(EA.Repository repository, bool end = true, bool history = false, bool initial = false)
        {
            isEnd_aState = end;
            isHistory_aState = history;
            isInitial_aState = initial;

            EA.Diagram diagram = repository.GetCurrentDiagram();
            // 現在のダイアグラムよりノードの情報を配列に読み込む
            length = diagram.DiagramObjects.Count;
            array = new StateNode[length];
            for (short doID = 0; doID < length; doID++)
            {
                EA.DiagramObject dobj = repository.GetCurrentDiagram().DiagramObjects.GetAt(doID);
                EA.Element element = repository.GetElementByID(dobj.ElementID);
                array[doID] = new StateNode(this, dobj, element);
                e2d.Add(dobj.ElementID, doID);
            }
            // And状態の設定を行う．
            foreach (StateNode sNode in array)
            {
                sNode.AndSetting();
            }
            // 重みの計算を行う．
            SetWeight();
        }
        public StateNode[] Array {  get { return array; } }
        public int Length { get { return length; } }

        // DiagramObjectの順に並ぶ配列Arrayに対し，
        // ElementのIDで指定して状態ノードを参照する
        public StateNode this[int eID]
        {
            get
            {
                if (e2d.ContainsKey(eID) == true)
                {
                    return array[e2d[eID]];
                }
                else
                {
                    return null;
                }
            }
        }

        // 重みの計算
        // トップの状態からスタートして，その内部の重みを決定する
        // 
        private void SetWeight ()
        {
            int count = 0;
            
            foreach (StateNode sNode in array)
            {
                if (sNode.IsRoot() == true && IsState(sNode) == true)
                    count++;
            }
            foreach (StateNode sNode in array)
            {
                if (sNode.IsRoot() == true && IsState(sNode) == true)
                    sNode.SetWeight(1.0 / count);
            }
        }
        // 状態として扱うかどうかの判定
        public bool IsState(StateNode sNode)
        {
            return (sNode.Type() == StateNodeType.Or ||
                    sNode.Type() == StateNodeType.Machine ||
                    (sNode.Type() == StateNodeType.End && isEnd_aState == true) ||
                    (sNode.Type() == StateNodeType.Initial && isInitial_aState == true) ||
                    (sNode.Type() == StateNodeType.History && isHistory_aState == true));
        }
        // 複雑度計算
        //
        // Leafの状態数を数える(デフォルトでは，End状態は数える
        public int CountLeafStates()
        {
            int count = 0;
            foreach (StateNode sNode in array)
            {
                if (sNode.IsLeaf() == true && IsState(sNode) == true)
                {
                    count++;
                }
            }
            return count;
        }

        // OR状態数を数える : And状態は数えない
        public int CountOrStates(bool end = true, bool history = false, bool initial = false)
        {
            int count = 0;
            foreach (StateNode sNode in array)
            {
                if (IsState(sNode) == true)
                {
                    count++;
                }
            }
            return count;
        }

        // 階層状態複雑度
        // 0はエラー
        public double CountStateComplexity(short eleID = 0)
        {
            double count = 0;
            if (eleID == 0) { // ダイアグラム全体の複雑度
                foreach (StateNode sNode in array)
                {
                    if (sNode.IsRoot() == true && IsState(sNode) == true)
                    {
                        // 要素があれば１と数え，その要素の
                        count += 1.0 + sNode.CountStateComplexy();
                    }
                }
            }
            else　// 要素を指定した複雑度
            {
                StateNode sNode = this[eleID];
                if (sNode != null && IsState(sNode) == true)
                    count = sNode.CountStateComplexy(true);
            }
            return count;
        }
        // 状態遷移複雑用：2状態間の接続線の重みの計算
        public double CalculateTransitionWeight(int startID, int endID)
        {
            double result = 0.0;

            StateNode sNode = this[startID];
            StateNode eNode = this[endID];

            // ノードがない場合は終わり
            if (sNode == null || eNode == null)
                return result;

            while (sNode != null && eNode != null && sNode.ParentNode != eNode.ParentNode)
            {
                double sParentWeight = sNode.ParentWeight();
                double eParentWeight = eNode.ParentWeight();

                if (sParentWeight > eParentWeight) {
                    result += eParentWeight;
                    eNode = eNode.ParentNode;
                }
                else if (sParentWeight < eParentWeight)
                {
                    result += sParentWeight;
                    sNode = sNode.ParentNode;
                }
                else
                {
                    result += sParentWeight + eParentWeight;
                    sNode = sNode.ParentNode;
                    eNode = eNode.ParentNode;
                }
            }
            return result + ((sNode == null) ? 0 : sNode.ParentWeight());
        }
        // 2つの状態間の接続線が，Or状態の壁を横断する数の計算
        // ロジックは，CalculateTransitionWeithtと同じ
        public int NumberOfCrossing(int startID, int endID)
        {
            int nCross = 0;
            StateNode sNode = this[startID];
            StateNode eNode = this[endID];

            while (sNode != null && eNode != null && sNode.ParentNode != eNode.ParentNode)
            {
                double sParentWeight = sNode.ParentWeight();
                double eParentWeight = eNode.ParentWeight();

                if (sParentWeight > eParentWeight)
                {
                    eNode = eNode.ParentNode;
                    nCross++;
                }
                else if (sParentWeight < eParentWeight)
                {
                    sNode = sNode.ParentNode;
                    nCross++;
                }
                else
                {
                    sNode = sNode.ParentNode;
                    eNode = eNode.ParentNode;
                    nCross += 2;
                }
            }
            return nCross;
        }
        public string AbsoluteStateName(int elementID)
        {
            return AbsoluteStateName(this[elementID]);
        }
        public string AbsoluteStateName(StateNode sNode)
        {
            string apath = "";
            if (sNode != null)
            {
                apath = sNode.Name;
                short andID = sNode.AndID;
                for (sNode = sNode.ParentNode; sNode != null; sNode = sNode.ParentNode)
                {
                    // AND状態の処理
                    if (sNode.HasAndStates == true)
                    {
                        apath = sNode.AndStateName(andID) + "." + apath;
                    }
                    andID = sNode.AndID;
                    // 親状態の処理
                    apath = sNode.Name + "." + apath;
                }
            }
            return apath;
        }
        // デバッグ用 すべてのLeaf状態に対するフルパスを返す
        public string APaths()
        {
            string apaths = "";
            for (int i = 0; i < array.Length; i++)
            {
                StateNode sNode = array[i];
                if (sNode != null && sNode.IsLeaf() == true)
                {
                    apaths += AbsoluteStateName(sNode.ElementID) + Environment.NewLine;
                }
            }
            return apaths;
        }
    }
}