﻿using System;
using System.Windows.Forms;

namespace testProject
{
    public class test
    {
        //　アドインの有効無効
        public void EA_GetMenuState(EA.Repository repository, string MenuLocation, string MenuName, string ItemName, ref bool IsEnabled, ref bool IsChecked)
        {
            EA.Diagram diagram = repository.GetCurrentDiagram();
            if (diagram == null || diagram.Type != "Statechart")
            {
                IsEnabled = false;
            }
            else
            {
                IsEnabled = true;
            }
        }

        //　アドインのメニュー項目名称
        public object EA_GetMenuItems(EA.Repository repository, string location, string menuName)
        {
            return "状態マシン図複雑度";
        }

        ///
        /// 独自アドインのメニュー項目を選択した場合の処理
        /// 
        public void EA_MenuClick(EA.Repository repository, string location, string menuName, string itemName)
        {
            /// 現在開いているダイアグラムを取得
            EA.Diagram diagram = repository.GetCurrentDiagram();

            /// 要素の準備
            StateNodeArray states = new StateNodeArray(repository);
            DependencyManager dm = new DependencyManager(states);
            TransitionArray transitions = new TransitionArray(repository, states, dm);


            System.Diagnostics.Debug.WriteLine(states.APaths());

            MessageBox.Show("---------------------" + Environment.NewLine
                    + "状態数        : " + states.CountOrStates() + Environment.NewLine
                    + "末端状態数    : " + states.CountLeafStates() + Environment.NewLine
                    + "階層状態複雑度: " + states.CountStateComplexity() + Environment.NewLine
                    + "---------------------" + Environment.NewLine
                    + "遷移数: " + transitions.CountTransitions() + Environment.NewLine
                    + "横断数超過数(2以上): " + transitions.CountTransitionsExceeding(2) + Environment.NewLine
                    + "状態遷移複雑度 : " + transitions.CountTransitionComplexity() + Environment.NewLine
                    + "サイクロマティック複雑度： " + (transitions.CountTransitions()-states.CountOrStates()+2) + Environment.NewLine
                    + "---------------------" + Environment.NewLine
                    + "イベントの数 : " + dm.CountEvent() + Environment.NewLine
                    + "変数の数　 : " + dm.CountVariable() + Environment.NewLine
                    + "状態間結合度（イベント）: " + dm.EventCoupling() + Environment.NewLine
                    + "状態間結合度（状態）　　: " + dm.StateCoupling() + Environment.NewLine
                    + "状態間結合度（変数）　　: " + dm.VariableCoupling() + Environment.NewLine
                    + "状態間結合度（合計）　　: " + dm.TotleCoupling(),
                    "複雑度");
        }
    }
}