﻿using System;

namespace testProject
{
    public class Outputer
    {
        int eID;
        DependencyManager dm;

	    public Outputer(int id, DependencyManager m)
	    {
            eID = id;
            dm = m;
	    }
        public void Put(string name, MediumType mt, AccessType at)
        {
            switch (mt)
            {
                case MediumType.Event:
                    dm.AppendEvent(eID, name, at);
                    break;
                case MediumType.State:
                    dm.AppendState(eID, name, at);
                    break;
                case MediumType.Variable:
                    dm.AppendVariable(eID, name, at);
                    break;
            }
        }
    }
}