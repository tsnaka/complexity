﻿using System;

namespace testProject
{
    public class CharBuffer
    {
        private string sbuffer;
        int ind;

        public CharBuffer(string str)
        {
            sbuffer = str;
            ind = 0;
        }
        public char Current { get { return (IsEnd) ? '\0' : sbuffer[ind]; } }
        public CharBuffer Next() { ind++; return this; }
        public CharBuffer Back() { ind--; return this; }
        public CharBuffer SkipSpace()
        {
            while (IsEnd == false && char.IsWhiteSpace(Current) == true)
                Next();
            return this;
        }
        public bool IsEnd { get { return (ind < sbuffer.Length) ? false : true; } }
        public bool StartWith(string s)
        {
            if (sbuffer.Length - ind >= s.Length)
                return sbuffer.Substring(ind, s.Length) == s;
            return false;
        }
        public int Length { get { return sbuffer.Length; } }
    }
}