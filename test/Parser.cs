﻿using System;

namespace testProject
{
    enum ParseScope { Event, Guard, Action,Undecided }
    public class Parser
    {
        CharBuffer cbuf;
        Outputer outputer;
        ParseScope scope;
        public bool Err;

        public Parser(string str, Outputer ot)
        {
            cbuf = new CharBuffer(str);
            scope = ParseScope.Undecided;
            Err = false;
            outputer = ot;
        }
        public bool EvalEvent()
        {
            if (cbuf.Length > 0)
            {
                scope = ParseScope.Event;
                Trigger(0);
                return !Err;
            }
            return true;
        }
        public bool EvalGuard()
        {
            if (cbuf.Length > 0)
            {
                scope = ParseScope.Guard;
                Lexpr(0);
                return !Err;
            }
            return true;
        }
        public bool EvalActions()
        {
            if (cbuf.Length > 0)
            {
                scope = ParseScope.Action;
                Actions(0);
                return !Err;
            }
            return true;
        }

        // For Guard
        // Lexpr  -> Lterm | Lterm {| Lterm}
        // Lterm  -> Lfact | l Lfact {& Lfact}
        // Lfact  -> (Lexpr) | Slexpr
        // Slexpr -> in ( Variable ) | Expr | Expr lop Expr 
        // Expr   -> Term | Term { + Term } | Term { - Term }
        // Term   -> Fact | Fact { * Fact } | Fact { / Fact } 
        // Fact   -> ( Expr ) | Var
        // Var    -> Number | Variable | Function ( Arguments )
        // Arguments -> Expr | Expr {, Expr}


        // Lexpr := Lterm | Lterm {| Lterm}
        private void Lexpr(int level)
        {
            ShowScope(level, "Lexpr");

            Lterm(level + 1);
            while (true)
            {
                cbuf.SkipSpace();
                if (cbuf.Current == '|')
                {
                    cbuf.Next();
                    Lterm(level + 1);
                }
                else
                {
                    break;
                }
            }
        }

        // Lterm := Lfact | Lfact {& Lfact}
        private void Lterm(int level)
        {
            ShowScope(level, "Lterm");

            Lfact(level + 1);

            while (true)
            {
                cbuf.SkipSpace();
                if (cbuf.Current == '&')
                {
                    cbuf.Next();
                    Lfact(level + 1);
                }
                else
                    break;
            }
        }

        // Lfact := (Lexpr) | Slexpr
        private void Lfact(int level)
        {
            ShowScope(level, "Lfact");

            cbuf.SkipSpace();
            if (cbuf.Current == '(')
            {
                cbuf.Next(); ;
                Lexpr(level + 1);

                cbuf.Next();
                if (cbuf.Current != ')')
                {
                    Error("')' is missing.");
                }
                cbuf.Next(); ;
            }
            else
            {
                Slexpr(level + 1);
            }
        }

        // Slexpr -> in ( Variable ) | Expr | Expr lop Expr 
        private void Slexpr(int level)
        {
            ShowScope(level, "Slexpr");

            Expr(level + 1);
            cbuf.SkipSpace();
            if (cbuf.StartWith("==") || cbuf.StartWith("!=") ||
                cbuf.StartWith(">=") || cbuf.StartWith("<="))
            {
                cbuf.Next().Next();
                Expr(level + 1);
            }
            else if (cbuf.StartWith(">") || cbuf.StartWith("<"))
            {
                cbuf.Next();
                Expr(level + 1);
            }
        }


        // For Actions
        //
        // Actions -> Action | Action { ;  Actions}
        // Action  -> Variable = Expr |
        //            Function ( Arguments ) |
        //            Object . Message ( Arguments ) |
        //            ! Event ( Arguments )

        // Actions -> Action | Action { ,  Actions}
        private void Actions(int level)
        {
            ShowScope(level, "Actions");

            Action(level + 1);
            while (true)
            {
                cbuf.SkipSpace();
                if (cbuf.Current == ',')
                {
                    cbuf.Next();
                    Action(level + 1);
                }
                else
                {
                    break;
                }
            }
        }

        // Action  -> Variable = Expr |
        //            Function ( Arguments ) |
        //            Object . Message ( Arguments ) |
        //            ! Event ( Arguments )

        private void Action(int level)
        {
            ShowScope(level, "Action");

            cbuf.SkipSpace();

            if (cbuf.Current == '!')
            {
                // ! Event ( Arguments )
                cbuf.Next();
                string vname = Id();
                cbuf.SkipSpace();
                ShowScope(level + 1, "Event:" + vname);

                if (cbuf.Current == '(')
                {
                    cbuf.Next();
                    Arguments(level + 1);

                    cbuf.SkipSpace();
                    if (cbuf.Current != ')') { 
                        Error("')' is missing.");
                    }   
                    cbuf.Next();
                }
                // イベントの発生の登録（Writeの参照）
                outputer.Put(vname, MediumType.Event, AccessType.Write);
            }
            else {
                string vname = Id();
                cbuf.SkipSpace();

                if (cbuf.Current == '=')
                {
                    // Variable = Expr
                    ShowScope(level + 1, "Variable: " + vname);
                    cbuf.Next();
                    Expr(level + 1);

                    // 変数への代入の登録（Writeの参照）
                    outputer.Put(vname, MediumType.Variable ,AccessType.Write);
                }
                else if (cbuf.Current == '.')
                {
                    // Object . Message ( Arguments )
                    cbuf.Next();
                    string mname = Id();
                    ShowScope(level + 1, "Object.Message: " + vname + "." + mname);
                }
                else if (cbuf.Current == '(')
                {
                    // Function ( Arguments ) 
                    cbuf.Next();
                    Arguments(level + 1);
                    ShowScope((level + 1), "Function: " + vname);
                }
                else
                {
                    Error("Illegal Action statements.");
                }
            }
        }
    

        // For Event
        //
        // Trigger-> Ename | Ename ( Arguments )
        // Ename  -> Id

        // Event  -> Ename | Ename ( Arguments )
        private void Trigger (int level)
        {
            ShowScope(level, "Event");

            Ename(level+1);
            cbuf.SkipSpace();
            if (cbuf.Current == '(') {
                cbuf.Next();
                Arguments(level + 1);
                cbuf.SkipSpace();
                if (cbuf.Current != ')')
                {
                    Error("')' is missing.");
                }
                cbuf.Next();
            }
        }

        // Ename -> Id
        private void Ename(int level)
        {
            ShowScope(level, "Ename");
            string vname = Id();
            ShowScope(level,vname);

            // イベントの参照の登録（Readの参照）
            outputer.Put(vname, MediumType.Event, AccessType.Read);
        }

        // Expr   -> Term | Term { + Term } | Term { - Term }
        private void Expr(int level)
        {
            ShowScope(level, "Expr");

            Term(level + 1);
            while (true)
            {
                cbuf.SkipSpace();
                if (cbuf.Current == '+' || cbuf.Current == '-')
                {
                    cbuf.Next();
                    Term(level + 1);
                }
                else
                {
                    break;
                }
            }
        }

        // Term   -> Fact | Fact { * Fact } | Fact { / Fact } 
        private void Term(int level)
        {
            ShowScope(level, "Term");

            Fact(level + 1);

            while (true)
            {
                cbuf.SkipSpace();
                if (cbuf.Current == '*' || cbuf.Current == '/')
                {
                    cbuf.Next();
                    Fact(level + 1);
                }
                else
                {
                    break;
                }
            }
        }

        // Fact   -> ( Expr ) | Var
        private void Fact(int level)
        {
            ShowScope(level, "Fact");

            cbuf.SkipSpace();
            if (cbuf.Current == '(')
            {
                cbuf.Next();
                Expr(level + 1);

                cbuf.SkipSpace();
                if (cbuf.Current != ')')
                {
                    Error("')' is missing.");
                }
                cbuf.Next();
            }
            else
            {
                Var(level + 1);
            }
        }

        // Var    -> name | name ( Arguments )
        private void Var(int level)
        {
            ShowScope(level, "Var");

            cbuf.SkipSpace();
            if (char.IsDigit(cbuf.Current))
            {
                double value = 0;
                while (char.IsDigit(cbuf.Current))
                {
                    value = value * 10 + (double)(cbuf.Current - '0');
                    cbuf.Next();
                }
                if (cbuf.Current == '.')
                {
                    int i = 10;
                    cbuf.Next();
                    while (char.IsDigit(cbuf.Current))
                    {
                        value += (double)(cbuf.Current - '0') / i;
                        i = i * 10;
                        cbuf.Next();
                    }
                }
                ShowScope(level + 1,"Number= " + value);
            }
            else if (char.IsLetter(cbuf.Current) )
            {
                string vname = "";
                vname += cbuf.Current;
                cbuf.Next();

                while (cbuf.IsEnd == false &&
                    (char.IsLetter(cbuf.Current) || char.IsNumber(cbuf.Current) || cbuf.Current == '_' ))
                {
                    vname += cbuf.Current;
                    cbuf.Next();
                }

                cbuf.SkipSpace();
                if (cbuf.Current == '(')
                {
                    if (vname == "in")
                    {
                        // 状態の参照の処理
                        if (scope == ParseScope.Guard)
                        {
                            cbuf.Next();
                            string sname = Id();
                            cbuf.SkipSpace();
                            if (cbuf.Current != ')')
                            {
                                Error("')' is missing.");
                            }
                            cbuf.Next();
                            ShowScope(level + 1, "in (" + sname + ")");

                            // 状態の参照の登録（Readの参照）
                            outputer.Put(sname, MediumType.State, AccessType.Read);
                        }
                        else
                            Error("Illegal use of 'in' statement");
                    }
                    else
                    {
                        cbuf.Next();
                        Arguments(level + 1);
                        cbuf.SkipSpace();
                        if (cbuf.Current != ')')
                        {
                            Error("')' is missing.");
                        }
                        cbuf.Next();
                        ShowScope(level + 1, "Function = " + vname);
                    }
                }
                else
                {
                    ShowScope(level + 1, "Valiable = " + vname);

                    // 変数の参照の登録（Readの参照）
                    outputer.Put(vname, MediumType.Variable, AccessType.Read);
                }
            }
            else
            {
                Error("Illegal Var : " + cbuf.Current);
            }
        }

        // Arguments -> Expr | Expr {, Expr}
        private void Arguments(int level)
        {
            ShowScope(level, "Arguments");
            cbuf.SkipSpace();

            Expr(level + 1);
            while (true)
            {
                cbuf.SkipSpace();
                if (cbuf.Current == ',')
                {
                    cbuf.Next();
                    Expr(level + 1);
                }
                else
                {
                    break;
                }
            }
        }

        private string Id()
        {
            string vname = "";
            cbuf.SkipSpace();
            if (cbuf.IsEnd == false && char.IsLetter(cbuf.Current))
            {
                vname += cbuf.Current;
                cbuf.Next();

                while (cbuf.IsEnd == false &&
                    (char.IsLetter(cbuf.Current) || char.IsNumber(cbuf.Current) || cbuf.Current == '_'))
                {
                    vname += cbuf.Current;
                    cbuf.Next();
                }
            }
            return vname;
        }

        private string PutIndent(int i)
        {
            string str = "";
            for (; i > 0; i--)
                str += " ";
            return str;
        }

        private void ShowScope(int i, string s)
        {
            System.Diagnostics.Debug.WriteLine(PutIndent(i) + s);
        }
        private void Error(string s)
        {
            System.Diagnostics.Debug.WriteLine(s);
            Err = true;
        }
    }
}
