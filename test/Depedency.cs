﻿using System;
using System.Collections.Generic;

namespace testProject
{
    public enum MediumType { Event, State, Variable }
    public enum AccessType { Read, Write }

    public class Dependency
    {
        int elementID;  // 状態遷移元の要素ID
        string mediumName;
        MediumType medium;
        AccessType access;

        public Dependency(int eID, string name, MediumType mt, AccessType at)
        {
            elementID = eID;
            mediumName = name;
            medium = mt;
            access = at;
        }
        public int ElementID { get { return elementID; } }
        public string MediumName { get { return mediumName; } }
        public MediumType Medium { get { return medium; } }
        public AccessType Access { get { return access; } }
    }
    public class DependencyManager
    {
        List<Dependency> dlist;
        Dictionary<string, MediumType> mediaDictionary;

        StateNodeArray sna;

        public DependencyManager(StateNodeArray array)
        {
            dlist = new List<Dependency>();
            mediaDictionary = new Dictionary<string, MediumType>();
            sna = array;

            // 状態は，記述されたものの参照になるため，辞書にはすべての状態（および状態マシン）ノードを登録
            // さらに，記述されたものは，Writeが1回されたものと考える（Readがなければ複雑度に影響なし）
            // in(状態名)は，単純な状態名で運用する場合（名称の一意性を仮定）と絶対パス名の場合に同時に対応
            for (int i = 0; i < sna.Length; i++)
            {
                StateNode sNode = sna.Array[i];
                if (sNode.Type() == StateNodeType.Or || 
                    sNode.Type() == StateNodeType.Machine)
                {
                    // 単純な状態名の登録
                    if (sNode.Name != null && mediaDictionary.ContainsKey(sNode.Name) == false)
                    {
                        mediaDictionary.Add(sNode.Name, MediumType.State);
                        dlist.Add(new Dependency(sNode.ElementID, sNode.Name, 
                            MediumType.State, AccessType.Write));
                    }
                    // 絶対パス名の登録
                    string apath = sna.AbsoluteStateName(sNode);
                    if (apath != null && mediaDictionary.ContainsKey(apath) == false)
                    {
                        mediaDictionary.Add(apath, MediumType.State);
                        dlist.Add(new Dependency(sNode.ElementID, apath,
                            MediumType.State, AccessType.Write));
                    }
                }
            }
            
        }
        // イベント参照の登録＋ 辞書になければ新たに登録
        public void AppendEvent(int eID, string name, AccessType at)
        {
            dlist.Add(new Dependency(eID, name, MediumType.Event, at));
            if (mediaDictionary.ContainsKey(name) == false)
                mediaDictionary.Add(name, MediumType.Event);
        }
        // 状態参照の登録
        public void AppendState(int eID, string name, AccessType at)
        {
            dlist.Add(new Dependency(eID, name, MediumType.State, at));
        }
        // 変数参照の登録＋ 辞書になければ新たに登録
        public void AppendVariable(int eID, string name, AccessType at)
        {
            dlist.Add(new Dependency(eID, name, MediumType.Variable, at));
            if (mediaDictionary.ContainsKey(name) == false)
                mediaDictionary.Add(name, MediumType.Variable);
        }
        //
        // 複雑度の計算
        //
        // イベントの数
        public int CountEvent()
        {
            int count = 0;
            foreach (var term in mediaDictionary)
            {
                if (term.Value == MediumType.Event)
                {
                    count++;
                    System.Diagnostics.Debug.WriteLine("Event[" + count + "] = " + term.Key);
                }
            }
            return count;
        }
        // 変数の数
        public int CountVariable()
        {
            int count = 0;
            foreach (var term in mediaDictionary)
            {
                if (term.Value == MediumType.Variable)
                {
                    count++;
                    System.Diagnostics.Debug.WriteLine("Variable[" + count + "] = " + term.Key);
                }
            }
            return count;
        }
        // 状態結合度
        // イベント，状態，変数　はUniverseにあり重みは1.0と考える．
        public double EventCoupling() { return CouplingBy(MediumType.Event);  }
        public double StateCoupling() { return CouplingBy(MediumType.State); }
        public double VariableCoupling() { return CouplingBy(MediumType.Variable); }
        public double TotleCoupling()
        {
            return EventCoupling()+ StateCoupling()+ VariableCoupling();
        }

        private double CouplingBy(MediumType type)
        {
            double result = 0.0;

            foreach (var pair in mediaDictionary)
            {
                int nRead = 0;
                int nWrite = 0;
                foreach (Dependency d in dlist)
                {
                    if (pair.Value == type &&
                        d.Medium == type && d.MediumName == pair.Key)
                    {
                        if (d.Access == AccessType.Read) nRead++;
                        else nWrite++;
                    }
                }
                result += nRead * nWrite;
            }
            return result;
        }
    }
}
