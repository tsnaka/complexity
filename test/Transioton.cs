﻿using System;

namespace testProject
{
    public enum TransitionType { Transition, NonTarget };

    public class Transition
    {
        EA.Connector cLink;
        public Transition(EA.Connector c)
        {
            cLink = c;
        }
        public TransitionType Type
        {
            get
            {
                return (cLink.Type == "StateFlow")
                    ? TransitionType.Transition
                    : TransitionType.NonTarget;
            }
        }
        public int StartElementID { get { return cLink.ClientID; } }
        public int EndElementID { get { return cLink.SupplierID; } }
        public bool Parse(DependencyManager dm)
        {
            Parser eparser = new Parser(cLink.TransitionEvent, new Outputer(StartElementID, dm));
            bool eresult = eparser.EvalEvent();
            if (eresult == false)
            {
                System.Diagnostics.Debug.WriteLine(" Event Parse Error : '" + cLink.TransitionEvent + "'");
            }

            Parser gparser = new Parser(cLink.TransitionGuard, new Outputer(StartElementID, dm));
            bool gresult = gparser.EvalGuard();
            if (gresult == false)
            {
                System.Diagnostics.Debug.WriteLine(" Guard Parse Error : '" + cLink.TransitionGuard + "'");
            }

            Parser aparser = new Parser(cLink.TransitionAction, new Outputer(StartElementID, dm));
            bool aresult = aparser.EvalActions();
            if (aresult == false)
            {
                System.Diagnostics.Debug.WriteLine(" Action Parse Error : '" + cLink.TransitionAction + "'");
            }

            return eresult && gresult && aresult;
        }
    }
    public class TransitionArray
    {
        Transition[] array;
        int length;
        StateNodeArray states;
        DependencyManager dm;

        public TransitionArray(EA.Repository repository, StateNodeArray sarray, DependencyManager m)
        {
            states = sarray;
            dm = m;

            EA.Diagram diagram = repository.GetCurrentDiagram();
            length = diagram.DiagramLinks.Count;
            array = new Transition[length];

            for (short i = 0; i < length; i++)
            {
                EA.DiagramLink diagramLink = diagram.DiagramLinks.GetAt(i);
                EA.Connector connector = repository.GetConnectorByID(diagramLink.ConnectorID);
                array[i] = new Transition(connector);
                array[i].Parse(dm);
            }
        }
        // Setter, Getter
        public int Length { get { return length; } }

        public Transition this[int index]
        {
            get
            {
                return (ok(index)) ? array[index] : null;
            }
        }
        private bool ok(int index)
        {
            return (index > 0 & index < Length) ? true : false;
        }
        // 複雑度計算
        //
        // 状態遷移の数
        public int CountTransitions()
        {
            int number = 0;
            for (int i = 0; i < Length; i++)
            {
                if (IsValid(array[i]) == true)
                    number++;
            }
            return number;
        }
        // 状態横断数が基準値(criterion)以上となった状態遷移数
        //
        public int CountTransitionsExceeding(int criterion)
        {
            int number = 0;
            for (int i = 0; i < Length; i++)
            {
                Transition tra = array[i];
                if (IsValid(tra) == true)
                {
                    if (states.NumberOfCrossing(tra.StartElementID, tra.EndElementID) >= criterion)
                        number++;
                }
            }
            return number;
        }
        // 階層状態遷移複雑度
        //
        public double CountTransitionComplexity()
        {
            double result = 0.0;
            for (int i = 0; i < Length; i++)
            {
                Transition tra = array[i];
                if (IsValid(tra) == true)
                    result += states.CalculateTransitionWeight(tra.StartElementID, tra.EndElementID);
            }
            return result;
        }
        // リンクが有効かどうかを判定する．
        // 初期状態からの遷移を入れることにしている．
        //
        private bool IsValid(Transition tra)
        {
            if (tra.Type == TransitionType.Transition)
            {
                if (states[tra.StartElementID] != null &&
                        states[tra.EndElementID] != null)
                    return true;
            }
            return false;
        }
    }
}
